<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css"/>
<nav>
  <?php 
  session_start();
   ?>
         <div class="menu-icon">
            <span class="fas fa-bars"></span>
         </div>
         <div class="logo">
          <a href="">
          <img src="logo.png">
          <p>Barokah<br>Auto Parts</p></a>
         </div>
         <form action="#">
            <input type="search" class="search-data" placeholder="Search" required>
         </form>
         <div class="nav-items">
            <?php if (isset($_SESSION['user_name'])) {?>
            <li><a href="#"><img src="image2.png"></a></li>
            <li><a href="#"><img src="image1.png"></a></li>
            <li><a href = "logout.php">KELUAR</a></li>
            <?php } ?>
            <?php if (!isset($_SESSION['user_name'])) {?>
            <li><a href="welcome.php"><p>Masuk</p></a></li>
            <li><a href="#"><p>Daftar</p></a></li>
          <?php }else{ ?>
            <li><a href="#"><img src="avatar.png" id="ava"></a></li>
            <li><p><?php echo $_SESSION['user_name'] ?></p></li>
            <div class="profile">
              <h1>Aktivitas</h1>
              <a href=""><img src="avatar.png"><p>Histori Transaksi</p></a>
              <a href=""><img src="avatar.png"><p>coba</p></a>
              <a href=""><img src="avatar.png"><p>Histori Transaksi</p></a>
              <a href=""><img src="avatar.png"><p>Histori Transaksi</p></a>
              <h1>Aktivitas</h1>
              <a href=""><img src="avatar.png"><p>Histori </p></a>
              <a href=""><img src="avatar.png"><p>Histori Transaksi</p></a>
              <a href=""><img src="avatar.png"><p> Transaksi</p></a>
              <h1>Aktivitas</h1>
              <a href=""><img src="avatar.png"><p>Histori Transaksi</p></a>
              <a href=""><img src="avatar.png"><p>Histori Transaksi</p></a>
              <a href="logout.php"><img src="avatar.png"><p>Logout</p></a>
            <?php } ?>
            </div>
         </div>
         <div class="search-icon">
            <span class="fas fa-search"></span>
         </div>
         <div class="cancel-icon">
            <span class="fas fa-times"></span>
         </div>
      </nav>
      <script>
         const ava = document.querySelector(".nav-items #ava");
         const profile = document.querySelector(".profile");
         const menuBtn = document.querySelector(".menu-icon span");
         const searchBtn = document.querySelector(".search-icon");
         const cancelBtn = document.querySelector(".cancel-icon");
         const items = document.querySelector(".nav-items");
         const form = document.querySelector("form");
         menuBtn.onclick = ()=>{
           items.classList.add("active");
           menuBtn.classList.add("hide");
           searchBtn.classList.add("hide");
           cancelBtn.classList.add("show");
         }
         cancelBtn.onclick = ()=>{
           items.classList.remove("active");
           menuBtn.classList.remove("hide");
           searchBtn.classList.remove("hide");
           cancelBtn.classList.remove("show");
           form.classList.remove("active");
           cancelBtn.style.color = "#ff3d00";
         }
         searchBtn.onclick = ()=>{
           form.classList.add("active");
           searchBtn.classList.add("hide");
           cancelBtn.classList.add("show");
         }
         ava.onclick=()=>{
          if(screen.width>1140){
          var elms = document.getElementsByClassName("profile");
          Array.from(elms).forEach((x) => {
            if (x.style.display === "none") {
              x.style.display = "block";
              x.style.position='absolute';
              x.style.top='15%';
            } else {
              x.style.display = "none";
            }})}
          }
      </script>
<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700&display=swap');

nav{
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: space-between;
  height: 70px;
  padding: 0 100px;
}
nav .logo img{
  height: 60px;
  float: left;
}
nav .logo p{
  margin: 10px;
  font-size: 15px;
  float: left;
}
nav .nav-items{
  display: flex;
  flex: 1;
  padding: 0 0 0 40px;
}
nav .nav-items li{
  list-style: none;
  padding: 0 15px;
}
nav .nav-items li img{
  float: left;
  height: 50px;
}
nav .nav-items li a{
  font-size: 18px;
  font-weight: 500;
  text-decoration: none;
}
nav .nav-items li p{
  margin-top: 10px;
  font-size: 22px;
}
nav .nav-items .profile{
  display: none;
}
nav .nav-items li a:hover{
  color: #ff3d00;
}
nav form{
  display: flex;
  height: 40px;
  margin-left: 20px;
  padding: 2px;
  background: #1e232b;
  min-width: 40%!important;
  border-radius: 2px;
  border: 1px solid rgba(155,155,155,0.2);
}
nav form .search-data{
  width: 100%;
  height: 100%;
  padding: 0 10px;
  font-size: 17px;
  border: none;
  font-weight: 500;
}
nav form button{
  padding: 0 15px;
  color: #fff;
  font-size: 17px;
  background: #ff3d00;
  border: none;
  border-radius: 2px;
  cursor: pointer;
}
nav form button:hover{
  background: #e63600;
}
nav .menu-icon,
nav .cancel-icon,
nav .search-icon{
  width: 40px;
  text-align: center;
  margin: 0 50px;
  font-size: 18px;
  cursor: pointer;
  display: none;
}
nav .menu-icon span,
nav .cancel-icon,
nav .search-icon{
  display: none;
}
  nav .nav-items .profile h1{
    font-size: 20px;
    display: block;
    text-align: left;
    margin: 10px;
  }
  nav .nav-items .profile a{
    text-decoration:none;
    display: block;
    margin: 10px;
  }
  nav .nav-items .profile a img{
    display: inline-block;
    height: 30px;
  }
  nav .nav-items .profile a p{
    display: inline-block;
    font-size: 22px;
    vertical-align: top;
    margin-left: 10px;
  }
@media (max-width: 1245px) {
  nav{
    padding: 0 50px;
  }
}
@media (max-width: 1140px){
  nav{
    padding: 0px;
  }
  nav .logo{
    flex: 2;
    text-align: center;
  }
  nav .nav-items{
    position: fixed;
    z-index: 99;
    top: 60px;
    width: 100%;
    left: -100%;
    height: 100%;
    padding: 10px 50px 0 50px;
    background: #ffffff;
    display: inline-block;
    transition: left 0.3s ease;
  }
  nav .nav-items.active{
    left: 0px;
  }  
  nav .nav-items.active li img{
    display: block;
    height: 40px;
    clear: both;
  }
  nav .nav-items.active li a p{
    font-size: 20px;
    margin-left: 15%;
    line-height: 20px;
    text-align: left;
  }
  nav .nav-items #ava{
    clear: both;
    position: absolute;
    margin-left: 8%;
    height: 80px;
  }
  nav .nav-items.active li p{
    font-size: 20px;
    margin-left: 20%;
    line-height: 20px;
    text-align: left;
    margin-top: 15px;
    display: block;
  }
  nav .nav-items.active .profile{
    display: block;
    position: absolute;
    top: 10%;
  }
  nav form{
    position: absolute;
    top: 80px;
    right: 50px;
    opacity: 0;
    pointer-events: none;
    transition: top 0.3s ease, opacity 0.1s ease;
  }
  nav form.active{
    top: 95px;
    opacity: 1;
    pointer-events: auto;
  }
  nav form:before{
    position: absolute;
    content: "";
    top: -13px;
    right: 0px;
    width: 0;
    height: 0;
    z-index: -1;
    border: 10px solid transparent;
    border-bottom-color: #1e232b;
    margin: -20px 0 0;
  }
  nav form:after{
    position: absolute;
    content: '';
    height: 60px;
    padding: 2px;
    background: #1e232b;
    border-radius: 2px;
    min-width: calc(100% + 20px);
    z-index: -2;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
  }
  nav .menu-icon{
    display: block;
  }
  nav .search-icon,
  nav .menu-icon span{
    display: block;
  }
  nav .menu-icon span.hide,
  nav .search-icon.hide{
    display: none;
  }
  nav .cancel-icon.show{
    display: block;
  }
}
nav .logo.space{
  color: red;
  padding: 0 5px 0 0;
}
@media (max-width: 980px){
  nav .menu-icon,
  nav .cancel-icon,
  nav .search-icon{
    margin: 0 20px;
  }
  nav form{
    right: 30px;
  }
  nav .nav-items #ava{
    margin-left: 8%;
  }
  nav .nav-items.active li p{
    margin-left: 23%;
  }
}
@media (max-width: 700px){
  nav .nav-items.active li p{
    margin-left: 28%;
  }
}
@media (max-width: 600px){
  nav .nav-items.active li p{
    margin-left: 35%;
  }
  nav .nav-items #ava{
    margin-left: 10%;
  }
}
@media (max-width: 500px){
  nav .menu-icon,
  nav .cancel-icon,
  nav .search-icon{
    margin: 0 10px;
    font-size: 16px;
  }
  nav .nav-items.active li p{
    margin-left: 45%;
  }
}
@media (max-width: 400px){
  nav .nav-items.active li p{
    margin-left: 60%;
  }
  nav .nav-items #ava{
    margin-left: 12%;
  }
}
.content{
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}
.content header{
  font-size: 30px;
  font-weight: 700;
}
.content .text{
  font-size: 30px;
  font-weight: 700;
}
.content .space{
  margin: 10px 0;
}

@media (max-height: 1000px){
  nav .nav-items.active .profile{
    top: 13%;
  }
}
@media (max-height: 700px){
  nav .nav-items.active .profile{
    top: 20%;
  }
}
@media (max-height: 550px){
  nav .nav-items.active .profile{
    top: 25%;
  }
}
@media (max-height: 400px){
  nav .nav-items.active .profile{
    top: 30%;
  }
}
</style>